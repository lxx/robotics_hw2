% File containing map information


% Formatting:
% wall x1 y1 x2 y2
%   Order does not matter between the points
% line x1 y1 x2 y2
% beacon x y [r g b] ID_tag
%   [r g b] is the red-green-blue color vector
% virtwall x y theta
%   Virtual walls emit from a location, not like real walls
%   theta is the angle relative to the positive x-axis


wall -0.284 4.350 -0.233 2.850
wall 1.267 1.510 1.440 -0.830
wall -3.233 4.410 -3.112 -4.090