%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMS W4733 Computational Aspects of Robotics 2015
%
% Homework 1
%
% Team number: 16
% Team leader: Ruilin Xu (rx2132)
% Team members: Qi Wang (qw2197), Haoxiang Xu (hx2185), He Li (hl2918)
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function  hw2_team_16(serPort)

    %==========================================================================
    % Variable Declaration
    %==========================================================================
    tStart= tic;                                        % Time limit marker
    maxDuration = 600;                                  % 600 seconds of max duration time    
    cur_distance = 0;                                   % Initialize Current Distance
    cur_angle = 0;                                      % Initialize Current Angle
    
    state = 'FOLLOW_LINE';                              % State of the robot: 'FOLLOW_LINE', 'FOLLOW_WALL', 'TURN', 'SUCCEED', 'FAIL'
    cur_x = 0;
    cur_y = 0;
    position_tolerance = 0.05;                           % Tolerance value when robot gets back to initial position
    position_residual = 0.2;
    position_firsttime = true;                          % Whether the robot reaches the initial position for the first time
    
    v_move_straight = 0.2;                              % Velocity of the robot when moving straight
    v_move_angle = 0.15;                                % Velocity of the robot when moving at an angle
    v_turn = 0.2;                                       % Speed of turning an angle
    
    no_bump_no_wall_firsttime = true;                   % Whether we lost the wall sensor and all bump sensors for the first time
    
    %==========================================================================
    % newly added variables
    % =========================================================================
    target_x = 4;                                       % Final Position, x-coordinate
    target_y = 0;                                       % Final Position, y-coordinate
    
    q_bump_x = 0;
    q_bump_y = 0;

    angle_turned = 0;
    new_follow_wall = true;
    allow_turn = true;
    
    %==========================================================================
    
    %==========================================================================
    % Helper Function Declaration
    %==========================================================================
    % Check if the robot is at 's' state
    function output = isState(s)
        output = strcmp(state, s);
    end
    
    % Set robot state to 's'
    function setState(s)
        state = s;
    end

    % moveAtAngle() moves the robot at 'angle' degrees at velocity 'v' and saves the total distance
    % Note: parameter 'v' is optional
    function moveAtAngle(radius, v)
        if (nargin < 2)
            v = 0.1;
        end
        
        % Move robot
        SetFwdVelRadiusRoomba(serPort, v, radius);
        pause(0.1);
        
        % Save current position and convert current position to the
        % original x-y coordinate system
        cur_distance = DistanceSensorRoomba(serPort);
        prev_angle = cur_angle;
        turned_angle = prev_angle + cur_distance / (2 * radius);
        cord_len = 2 * radius * sin(cur_distance / (2 * radius));
        cur_x = cur_x + cord_len * cos(turned_angle);
        cur_y = cur_y + cord_len * sin(turned_angle);
        cur_angle = cur_angle + AngleSensorRoomba(serPort);
        display(sprintf('moveAtAngle\ncur_distance: %g\ncur_angle: %g\ncur_pos: [%g, %g]\n', cur_distance, cur_angle, cur_x, cur_y));
    end

    % moveForward(v) moves the robot straight ahead at velocity 'v' and saves the total distance
    % Note: parameter 'v' is optional
    function moveForward(v)
        if (nargin < 1)
            v = 0.1;
        end
        
        % Reset no_bump_no_wall_firsttime
        no_bump_no_wall_firsttime = true;
        
        % Move robot
        SetFwdVelRadiusRoomba(serPort, v, inf);
        pause(0.1);
        
        % Save current position and convert current position to the
        % original x-y coordinate system
        cur_distance = DistanceSensorRoomba(serPort);
        cur_angle = cur_angle + AngleSensorRoomba(serPort);
        cur_x = cur_x + cur_distance * cos(cur_angle);
        cur_y = cur_y + cur_distance * sin(cur_angle);
        display(sprintf('moveForward\ncur_distance: %g\ncur_angle: %g\ncur_pos: [%g, %g]\n', cur_distance, cur_angle, cur_x, cur_y));
    end

    % turnAtAngle(angle, speed) turns the robot at 'angle' degrees at speed 'speed'
    % Note: parameter 'speed' is optional
    function turnAtAngle(angle, speed)
        if (nargin < 2)
            speed = 0.1;
        end
        
        angle = modifyAngle(angle);
        while(1)
            if(angle > 90)
                angle = angle - 90;
                turnAngle(serPort, speed, 90);
                SetFwdVelRadiusRoomba(serPort, 0, inf);
                pause(0.1);
            else
                turnAngle(serPort, speed, angle);
                SetFwdVelRadiusRoomba(serPort, 0, inf);
                pause(0.1);
                break;
            end
        end
        
        % Save current position and convert current position to the
        % original x-y coordinate system
        cur_distance = DistanceSensorRoomba(serPort);
        cur_angle = cur_angle + AngleSensorRoomba(serPort);
        cur_x = cur_x + cur_distance * cos(cur_angle);
        cur_y = cur_y + cur_distance * sin(cur_angle);
        display(sprintf('turnAtAngle\ncur_distance: %g\ncur_angle: %g\ncur_pos: [%g, %g]\n', cur_distance, cur_angle, cur_x, cur_y));
    end

    % walkAlongTheWall() lets the robot move along the wall
    function walkAlongTheWall()
        if (Bump)
            if (WallSensor)
                turnAtAngle(3, v_turn);
            else
                if (BumpLeft)
                    turnAtAngle(130, v_turn);
                elseif (BumpFront)
                    turnAtAngle(70, v_turn);
                elseif (BumpRight)
                    moveAtAngle(0.15, v_move_angle);
                end
            end
        else
            if (WallSensor)
                moveForward(v_move_straight);
            else
                if (no_bump_no_wall_firsttime)
                    moveForward(v_move_straight);
                    no_bump_no_wall_firsttime = false;
                else
                    moveAtAngle(-0.1, v_move_angle);
                end
            end
        end
    end

    % stopRoomba() stops the robot and display the total distance travelled
    function stopRoomba()
        % Stop the Robot
        SetFwdVelRadiusRoomba(serPort, 0, inf);
    end

    % =========================================================================
    % newly added functions
    %==========================================================================
    
    % follow the line, automatically erase the difference
    function followTheLine()
        if(cur_x-target_x >= 0)
            tempAngle = modifyRadian(cur_angle);
            if(tempAngle ~= pi)
                if(tempAngle < pi)
                    moveAtAngle(2, v_move_angle);
                else
                    moveAtAngle(-2, v_move_angle);
                end
            else 
                moveForward(v_move_straight);
            end
        elseif(cur_x-target_x < 0)
            if(cur_angle ~= 0)
                if(cur_angle < 0)
                    moveAtAngle(2, v_move_angle);
                else
                    moveAtAngle(-2, v_move_angle);
                end
            else 
                moveForward(v_move_straight);
            end
        end
    end
    
    % check if we have reached the goal
    function output = checkSucceed()
        if (abs(abs(cur_x)-target_x) <= position_residual && abs(abs(cur_y)-target_y) <= position_residual)
            output = true;
        else
            output = false;
        end
    end
    
    % save
    function saveBumpedPosition()
        q_bump_x = cur_x;
        q_bump_y = cur_y;
    end

    % checkTermination() decides whether the robot reaches its initial position
    function output = checkFail()
        % Check whether the termination conditions are met
        if (abs(abs(cur_x)-q_bump_x) <= position_residual && abs(abs(cur_y)-q_bump_y) <= position_residual)
            if (~position_firsttime)
                display('Reached the initial position! STOP!')
                setState('FAIL');
                output = true;
            else
                output = false;
            end
        else
            position_firsttime = false;
            output = false;
        end
    end

    % check whether the robot meet the line again
    function output = checkMeetMLine()
        if (position_firsttime)
            output = false;
        else
            if (abs(abs(cur_y)-target_y) <= position_tolerance) % Unwise.
                output = true;
            else
                output = false;
            end
        end
    end

    % check if we come closer
    function output = checkCloser()
        if (((cur_x-target_x)^2 + (cur_y-target_y)^2) < ((q_bump_x-target_x)^2 + (q_bump_y-target_y)^2)) % Unwise. 
            output = true;
        else
            output = false;
        end
    end

    function output = CheckImpeded()
        angle_turned = cur_angle * 180 / pi;
        
        if ((cur_x - target_x) > 0)
            angle_turned = modifyAngle(180 - angle_turned);
            if(mod(angle_turned,360) < 180)
                output = true;
            else
                output = false;
            end
        else
            angle_turned = modifyAngle(-angle_turned);
            if(mod(angle_turned,360) < 180) 
                output = true;
            else
                output = false;
            end
        end
    end

    function output = modifyAngle(angle)
       while(angle > 360)
           angle = angle - 360;
       end
       
       while(angle <= 0)
           angle = angle + 360;
       end
       
       output = angle;
    end

    function output = modifyRadian(radian)
        while(radian > 2*pi)
           radian = radian - 2*pi;
       end
       
       while(radian <= 0)
           radian = radian + 2*pi;
       end
       
       output = radian;
    end
    
    %==========================================================================
    % Main Work
    %==========================================================================
    while toc(tStart) < maxDuration
        % Get all data we need
        [BumpRight, BumpLeft, ~, ~, ~, BumpFront] = BumpsWheelDropsSensorsRoomba(serPort);
        WallSensor = WallSensorReadRoomba(serPort);
        Bump = BumpRight || BumpLeft || BumpFront;
                
%       % All situations
        if (isState('FOLLOW_LINE'))
            if(checkSucceed())
               setState('SUCCEED');
            elseif(Bump)
               position_firsttime = true;
               saveBumpedPosition();              
               setState('FOLLOW_WALL');
               new_follow_wall = true;
               display(sprintf('Bumped Position: [%g, %g]', q_bump_x, q_bump_y));
            else
                followTheLine();
            end
        elseif (isState('FOLLOW_WALL'))
            if(checkSucceed())
                setState('SUCCEED');
                continue;
            elseif(checkFail())
                setState('FAIL');
                continue;
            elseif(checkMeetMLine() && checkCloser() && allow_turn)
                if (~new_follow_wall)
                    setState('TURN');
                    continue;
                end
            end
            new_follow_wall = false;
            allow_turn = true;
            walkAlongTheWall();    
            
        elseif (isState('TURN'))
            if(CheckImpeded())
                turnAtAngle(angle_turned, v_turn);
                setState('FOLLOW_LINE');
%                 if(angle_turned > 90)
%                     turnAtAngle(90, v_turn);
%                 else
%                     turnAtAngle(angle_turned, v_turn);
%                     setState('FOLLOW_LINE');
%                 end
            else
                allow_turn = false;
                setState('FOLLOW_WALL');
            end
                
        elseif (isState('FAIL') || isState('SUCCEED'))
            disp('end');
            stopRoomba();
            break;
        end
    end
    %==========================================================================
    
end