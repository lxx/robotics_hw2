% File containing map information


% Formatting:
% wall x1 y1 x2 y2
%   Order does not matter between the points
% line x1 y1 x2 y2
% beacon x y [r g b] ID_tag
%   [r g b] is the red-green-blue color vector
% virtwall x y theta
%   Virtual walls emit from a location, not like real walls
%   theta is the angle relative to the positive x-axis


wall -3.164 -1.810 2.784 -1.670
wall 2.784 -1.670 2.733 -1.150
wall 2.733 -1.150 -3.095 -1.330
wall -3.095 -1.330 -3.181 -1.810
wall -0.216 -0.370 1.888 -0.230
wall 1.888 -0.230 1.750 2.390
wall 1.750 2.390 -0.267 2.310
wall -0.267 2.310 -0.233 1.690
wall -0.233 1.690 1.388 1.870
wall 1.388 1.870 1.457 0.130
wall 1.457 0.130 -0.181 0.130
wall -0.181 0.130 -0.216 -0.370
